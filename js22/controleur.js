
// ======================================================================================================================
// Implémentation de la classe qui permet un contrôle interactif de la caméra virtuelle
// ======================================================================================================================

import * as THREE from '../three/r120/build/three.module.js' ;
import PRIMS3D from './prims3d.js' ;
var PI_2 = Math.PI / 2.0 ;

var ControleurCamera = function(simulateur,object){

	this.simulateur      = simulateur ;
	this.object          = object ;
	this.container       =  document.getElementById('container') ;
	this.blocker         =  document.getElementById('blocker') ;
	this.controlsEnabled = false ;
	this.nombre_elem_servante = 0;
	this.listDesserte = [0,0,0,0,0,0,0,0];

	this.current_position="";
	this.list_cables= new Array();
	this.first_borne=null;
	this.second_borne=null;
	this.first_dipole=null;
	this.interupteur = null;
	this.second_dipole=null;
	this.signe_1=null;
	this.signe_2=null;
	// Le repère de l objet
	this.position  = new THREE.Vector3(6,1.7,9) ;
	this.right     = new THREE.Vector3() ;
	this.up        = new THREE.Vector3() ;
	this.at        = new THREE.Vector3() ;

	this.euler      = new THREE.Euler(0.0,0.0,0.0,'YXZ') ;
	this.quaternion = new THREE.Quaternion() ;

	this.object.matrix.extractBasis(this.right, this.up, this.at) ;

	this.angle     = 0.0 ;
	this.direction = new THREE.Vector3(1,0,0) ;
	this.cible     = new THREE.Vector3(2,1.7,5) ;

	this.angle1 = PI_2;
	this.angle2 = - PI_2;

	this.vitesse   = 5.0 ;

	this.vitesseAngulaireMax = 0.05 ;
	this.tauxRotation        = 0.0  ;
	this.controlActif        = false ;


	this.plusHaut  = false ;
	this.plusBas   = false ;
	this.enAvant   = false ;
	this.enArriere = false ;
	this.aGauche   = false ;
	this.aDroite   = false ;

	this.mouseClicked = false ;
	this.mouse     = new THREE.Vector2() ;
	this.raycaster = new THREE.Raycaster() ;

	this.world = null ;
	this.origin = new THREE.Vector3() ;
	this.ext = new THREE.Vector3() ;

	this.getPointerLock() ;

	this.activerClavier() ;
	this.PointerDown();
	this.PointerUp();
	this.RightClick();
	this.PointerMove();

}

var plane = new THREE.Plane();
var pNormal = new THREE.Vector3(0, 1, 0); // plane's normal
var planeIntersect = new THREE.Vector3(); // point of intersection with the plane
var pIntersect = new THREE.Vector3(); // point of intersection with an object (plane's point)
var shift = new THREE.Vector3(); // distance between position of an object and points of intersection with the object
var isDragging = false;
var correct = new THREE.Vector3();
var dragObject;
var objects_inter = [];
var moteurCord = new THREE.Vector3(-1,1.1,0); //boite_moteur
var ampouleCord = new THREE.Vector3(0.5,1.1,0); //boite_ampoule
var ampouleoffCord = new THREE.Vector3(1.5,1.1,0); //boite_ampoule_off
var pileCord = new THREE.Vector3(1.0,0.1,0); //boite_pile
var resistCord = new THREE.Vector3(-1,2.1,0); //boite_resistance
var ledCord = new THREE.Vector3(1.5,2.1,0); //boite_led
var interCord = new THREE.Vector3(0.5,2.1,0); //boite_interrupteur
var boiteCord = new THREE.Vector3(-1,0.1,0); //boite
var Cords = [
	["boite_moteur", moteurCord],
	["boite_ampoule",ampouleCord],
	["boite_ampoule_off",ampouleoffCord],
	["boite_pile",pileCord],
	["boite_resistance",resistCord],
	["boite_led",ledCord],
	["boite_interrupteur",interCord]
   ];
var desserte = null;

ControleurCamera.prototype.addToListDesserte = function (selected_object) {
	if (desserte == null)
		desserte = this.simulateur.scene.children.find(children => children.name === 'desserte');
	if (selected_object.type == "boite_elec") {
		selected_object.parent = desserte;
		for (let index = 0; index < this.listDesserte.length; index++) {
			if(this.listDesserte[index] === 0) {
				this.listDesserte[index] = selected_object;
				if (index <= 3) {
						selected_object.position.x = -0.3;
						selected_object.position.z = -1.25 + 0.5 * (index+1);
					} else {
						selected_object.position.x = 0.3;
						selected_object.position.z = -2 - 1.25 + 0.5 * (index+1);
					}
					selected_object.position.y = 0.04;
					break;
			}
			
		}
	}
}

ControleurCamera.prototype.RightClick = function(){
	document.addEventListener("mousedown", event => {
		if (event.button == 2){
		console.log("Right click : desserte -> etagere");
		var objects_inter = [];
		if (objects_inter.length == 0){
			for ( var i = 0; i < this.simulateur.scene.children.length; i++ ) {
				if(this.simulateur.scene.children[i].name=="etagere"){
					for ( var j = 0; j < this.simulateur.scene.children[i].children.length; j++ ) {
						if (this.simulateur.scene.children[i].children[j].type == "boite_elec"){
							objects_inter.push(this.simulateur.scene.children[i].children[j]);
						}
					}
				}
			}	
		}
		if (this.current_position == "table"){
			this.mouse.x = 0 ;
			this.mouse.y = 0 ;
			this.raycaster.setFromCamera(this.mouse, this.simulateur.camera)
			var intersects = this.raycaster.intersectObjects(objects_inter,true);
			// if (intersects.length > 0) {
			// 	console.log("there is inter");
			// 	console.log(intersects[0].object.parent.type);
			// 		if (intersects[0].object.parent.type == "desserte"){
			// 			console.log("Object found");
			// 		}
			// }
		}
		var et = this.simulateur.scene.children.find( children => children.name === 'etagere');
		//var dess = this.simulateur.scene.children.find( children => children.name === 'desserte');

		//tb.children.pop(); // remove object from table childrens
		intersects[0].object.parent.parent=et;
		//intersects[0].object.parent = table;table.add(selected_object);
		//tb.remove(intersects[0].object);
		//intersects[0].object.parent.add(et);

		for ( var i = 0; i < Cords.length; i++ ) {
			if (Cords[i][0] == intersects[0].object.parent.name ){
				intersects[0].object.parent.position.x =Cords[i][1].x;
				intersects[0].object.parent.position.y =Cords[i][1].y;
				intersects[0].object.parent.position.z =Cords[i][1].z;
			}
		}
		/*if (this.current_position == "table" &&  ){
			this.mouse.x = 0 ;
			this.mouse.y = 0 ;
			this.raycaster.setFromCamera(this.mouse, this.simulateur.camera)
			var intersects = this.raycaster.intersectObjects(objects_inter,true);
			if (intersects.length > 0) {
				console.log("there is inter");
				console.log(intersects[0].object.parent.type);
					if (intersects[0].object.parent.type == "desserte"){
						console.log("Object found");
						this.indice++;
					}
			}
		}*/
		/*intersects[0].object.parent.position.x=1.5;   //1.5;
		intersects[0].object.parent.position.y=1.1;
		intersects[0].object.parent.position.z=0*this.indice;   //1.1;
		console.log("Z:"+intersects[0].object.parent.position.z);*/
						}
	});
}

//collision
var MovingMesh;
var collidableObjectList = {};
var move = true;


ControleurCamera.prototype.PointerDown = function(){
	document.addEventListener("pointerdown", event => {
		//if (objects_inter.length == 0){
		collidableObjectList = [];
			for ( var i = 0; i < this.simulateur.scene.children.length; i++ ) {
				//console.log(this.simulateur.scene);
				if(this.simulateur.scene.children[i].name=="etagere"){
					for ( var j = 0; j < this.simulateur.scene.children[i].children.length; j++ ) {
						if (this.simulateur.scene.children[i].children[j].name.startsWith("boite_")){
							objects_inter.push(this.simulateur.scene.children[i].children[j]);
							//collision
							//collidableObjectList.push(this.simulateur.scene.children[i].children[j]
							//collidableObjectList.push(this.simulateur.scene.children[i].children[j].children.find( children => children.name === 'boite' && children.type == 'Mesh'));
							collidableObjectList[this.simulateur.scene.children[i].children[j].name] = (new THREE.Box3().setFromObject(this.simulateur.scene.children[i].children[j]));
						}
					}
				}
			}
		//}
		if (this.current_position == "table"){
			this.mouse.x = 0 ;
			this.mouse.y = 0 ;
			this.raycaster.setFromCamera(this.mouse, this.simulateur.camera)
			var intersects = this.raycaster.intersectObjects(objects_inter,true);
			if (intersects.length > 0) {
					if (intersects[0].object.parent.type == "boite_elec"){
						var _selected = intersects[0];
						pIntersect.copy(_selected.point);
						plane.setFromNormalAndCoplanarPoint(pNormal, pIntersect);
						shift.subVectors(_selected.object.parent.position, _selected.point);
						isDragging = true;
						dragObject = _selected.object.parent;
						MovingMesh = _selected.object;
						if (_selected.object.parent.parent.name == 'desserte' || _selected.object.parent.name == 'desserte') {
							for (var i = 0; i < this.listDesserte.length; i++) {
								if (this.listDesserte[i] === _selected.object.parent) {
									this.listDesserte[i] = 0;
									
								}
							}
						}
	
					}
			}
		}
	});
}

ControleurCamera.prototype.PointerUp = function(){
	document.addEventListener("pointerup", event => {
		//console.log("Pointer up");
		isDragging = false;
		dragObject = null;
	});
}

ControleurCamera.prototype.PointerMove = function(){
	document.addEventListener("pointermove", event => {
		this.mouse.x = 0;
		this.mouse.y = 0;
		// this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
		// this.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
		raycaster.setFromCamera(this.mouse, this.object);
		if (isDragging) {
			if (move) {
				raycaster.ray.intersectPlane(plane, planeIntersect);
				correct.x = dragObject.position.x;
				correct.y = dragObject.position.y;
				correct.z = dragObject.position.z;
				dragObject.position.addVectors(planeIntersect, shift);
				dragObject.parent.name = 'table';
			}
			move = true;
			collidableObjectList[dragObject.name] = null;
			//collision	
			var bbox = new THREE.Box3().setFromObject(dragObject);
			for (var k in collidableObjectList){
				if (k  == dragObject.name)
					continue;
				if (bbox.intersectsBox(collidableObjectList[k])){
					move = false;
					dragObject.position.x = correct.x;
					dragObject.position.y = correct.y;
					dragObject.position.z = correct.z;
					break;
				}
			}
		}
		
	});
}

ControleurCamera.prototype.getPointerLock = function(){
	var that = this ;
	document.addEventListener('pointerlockchange',function(){that.lockChange();}, false) ;
	document.addEventListener('mousemove',function(e){that.onMouseMove(e);}, false);
	document.onclick = function(e){
		document.body.requestPointerLock() ;
		that.mouseDown(e);
	}
}

ControleurCamera.prototype.goTableau =function(){
	this.position = new THREE.Vector3(9,1.7,3);
	this.angle1 = PI_2 + 0.2;
	this.angle2 = - PI_2 +0.2;
	this.current_position="tableau"
}
ControleurCamera.prototype.goEtagere =function(){

	this.position = new THREE.Vector3(5,2.3,5);
	this.angle1 = PI_2 + PI_2/2 +0.15;
	this.angle2 = - PI_2-0.2;
	this.current_position="etagere"
}
ControleurCamera.prototype.goTable =function(){
	this.position = new THREE.Vector3(8,2.5,6);
	this.angle1 = PI_2;
	this.angle2 = -(PI_2 + PI_2/2);
	this.current_position="table"
}
ControleurCamera.prototype.lockChange = function(){

    if (document.pointerLockElement === document.body) {
        // Hide blocker and instructions
        this.blocker.style.display = "none";
        this.controlsEnabled = true;
    // Turn off the controls
    } else {
      // Display the blocker and instruction
        this.blocker.style.display = "";
        this.controlsEnabled = false;
    }
}

ControleurCamera.prototype.onMouseMove = function(e){
	if(this.controlsEnabled==false) return ;

	var dx = e.movementX || e.mozMovementX || e.webkitMovementX || 0 ;
	var dy = e.movementY || e.mozMovementY || e.webkitMovementY || 0 ;
 	this.angle1 -= dx*0.002 ;

	if (this.angle2 - dy * 0.002 <= 0 && this.angle2 - dy * 0.002 > -3.0) {
		this.angle2 -= dy * 0.002 ;
	}
}


ControleurCamera.prototype.update = function(dt){

	this.object.matrix.extractBasis(this.right, this.up, this.at) ;

	if(this.plusHaut)
		this.position.y += this.vitesse * dt ;

	if(this.plusBas)
		this.position.y -= this.vitesse * dt ;

	if(this.aGauche)
		//this.angle += 0.05 ;
		this.position.addScaledVector(this.right,-this.vitesse*dt) ;

	if(this.aDroite)
		//this.angle -= 0.05 ;
		this.position.addScaledVector(this.right,this.vitesse*dt) ;

	if(this.enAvant){
		this.position.x +=  this.vitesse * dt * Math.cos(this.angle1) ;
		this.position.z += -this.vitesse * dt * Math.sin(this.angle1) ;
	}

	if(this.enArriere){
		this.position.x -=  this.vitesse * dt * Math.cos(this.angle1) ;
		this.position.z -= -this.vitesse * dt * Math.sin(this.angle1) ;
	}

	this.object.position.copy(this.position) ;

	this.direction.set(Math.cos(this.angle),0.0,-Math.sin(this.angle1)) ;


	if(mouseClicked) {
		this.object.position.set(ext.x,ext.y,ext.z);
		this.position.set(ext.x,ext.y,ext.z);
		this.cible.set(origin.x,origin.y,origin.z);
		this.direction.set(origin.x-ext.x,origin.y-ext.y,origin.z-ext.z) ;
		this.angle = -Math.atan2(this.direction.z,this.direction.x);

		mouseClicked = false ;

	} else {
			this.cible.set(this.position.x + Math.cos(this.angle1),
				this.position.y + Math.cos(this.angle2)*1.5,
				this.position.z - Math.sin(this.angle1))
	};
	this.object.lookAt(this.cible) ;

}

ControleurCamera.prototype.keyUp = function(event){
	switch(event.keyCode){
		case 33 : // HAUT
			this.plusHaut = false ;
			break ;
		case 34 : // BAS
			this.plusBas = false ;
			break ;
		case 37 : // GAUCHE
			this.aGauche = false ;
			break ;
		case 38 : // HAUT
			this.enAvant = false ;
			break ;
		case 39 : // DROITE
			this.aDroite = false ;
			break ;
		case 40 : // BAS
			this.enArriere = false ;
			break ;
	}
}



ControleurCamera.prototype.keyDown = function(event){
	switch(event.keyCode){
		case 33 : // HAUT
			this.plusHaut = true ;
			break ;
		case 34 : // BAS
			this.plusBas = true ;
			break ;
		case 49 :
			this.goTable();
			break;
		case 50 :
			this.goEtagere();
			break;
		case 51 :
			this.goTableau();
			break;
	}
}

var mouse     = new THREE.Vector2() ;
var raycaster = new THREE.Raycaster() ;
var mouseClicked = false ;
var world = null ;
var origin = new THREE.Vector3() ;
var ext = new THREE.Vector3() ;



// var mx, my, mdx, mdy ;

ControleurCamera.prototype.mouseMove = function(event){

	//console.log("MOUSE MOVE") ;
  /*
  event.preventDefault() ;
  mx  = (event.clientX/window.innerWidth)*2-1 ;
  my  = (-event.clientY/window.innerHeight)*2+1 ;
  mdx = event.movementX ;
  mdy = event.movementY ;

  if(mdx >  1 && mx > 0) {this.aDroite = true ;  this.aGauche = false} else
  if(mdx < -1 && mx < 0){this.aDroite = false ; this.aGauche = true} else
  {this.aDroite = false ; this.aGauche = false};
  */
  /*
  if(event.movementX > 5){
    controls.aDroite = true ;
    controls.aGauche = false ;
  } else if(event.movementX < -5){
    controls.aDroite = false ;
    controls.aGauche = true ;
  }
  */
  	

}

ControleurCamera.prototype.mouseDown = function(event){
	event.preventDefault() ;
	this.mouse.x = 0 ;
	this.mouse.y = 0 ;
	this.raycaster.setFromCamera(this.mouse, this.simulateur.camera) ;
	var objects = [];
	for ( var i = 0; i < this.simulateur.scene.children.length; i++ ) {
		if(this.simulateur.scene.children[i].type=="Group")
		{
			objects.push(this.simulateur.scene.children[i]);
		}
	}
	var intersects = this.raycaster.intersectObjects(objects,true);
	if(intersects.length>0 && !isDragging){
		//this.mouseClicked = true ;
		for (var i = 0; i < intersects.length; i++ ) {
			if (intersects[i].object.parent.name=="etagere" && this.current_position!="etagere") {
				//se deplacer sur l'etagere
				this.goEtagere();
			}
			if (intersects[i].object.parent.name=="table" && this.current_position!="table") {
				//se deplacer sur la table
				this.goTable();
			}
			// if (intersects[i].object.parent.name=="poster"&& this.current_position!="poster")
			// {
				//se deplacer sur le poster
				//this.goTableau();
			// }
			if (intersects[i].object.parent.type == "boite_elec" ){//|| intersects[i].object.parent.parent.parent.type == "boite_elec"){
				var selected_object = null;
				if (this.current_position == "etagere") {
					// click on boite
					if (intersects[i].object.parent.type == "boite_elec") {
						selected_object = intersects[i].object.parent;
						
					}
					// click on object
					else if(intersects[i].object.parent.parent.parent.type == "boite_elec") {
						selected_object = intersects[i].object.parent.parent.parent;
					}
					if(selected_object.parent.name=="etagere" || selected_object.parent.parent.name=="etagere") {
						this.addToListDesserte(selected_object);
					}
				}
			}
			if (intersects[i].object.name=="bouton" && intersects[i].object.parent.parent.parent.name=="table") {
				//Changement de position du switch
				this.interupteur = this.simulateur.dipoles["Interrupteur"];
				if(intersects[i].object.on==1)
				{
					
					intersects[i].object.rotation.y = Math.PI;
					intersects[i].object.on=0;
					this.interupteur.ouvert= true;
					console.log(this.interupteur);

				}
				else if(intersects[i].object.on==0)
				{
					intersects[i].object.rotation.y = 0;
					intersects[i].object.on=1;
					this.interupteur.ouvert= false;
					console.log(this.interupteur);

				}
			}
			//cable
			if(this.current_position == "table"){
			var draw = false;
				var element_clicked = intersects[i].object
				if ((element_clicked.parent.position.x < 0.75 || element_clicked.parent.position.x > 3.65 || element_clicked.parent.position.z > 0.85 || element_clicked.parent.position.z < -0.85)) {
					if (element_clicked.parent.type == "boite_elec") {
						//this.addToListDesserte(element_clicked.parent)
					}
				}

				if(element_clicked.type == "borne") {
					if(this.first_borne == null && element_clicked.parent?.parent?.name == "table")
					{
						if(element_clicked.nbr_cables<2){
							this.first_borne = intersects[i];

							var tempo = this.first_borne.object.parent.children.find(children => children.type === 'Group');
							this.first_dipole = this.simulateur.dipoles[tempo.name];

							if (this.first_borne.object.name.includes("1"))
								this.signe_1 = "+";
							else
								this.signe_1 = "-";
							
						} else{
							// console.log("you can't put more than 2 cables ")
							this.first_borne = null;
							this.second_borne = null;
						}
					} else {
						if (this.first_borne.object != element_clicked) {
							if (element_clicked.nbr_cables<2 ) {
								this.second_borne = intersects[i];

								tempo = this.second_borne.object.parent.children.find(children => children.type === 'Group');
								this.second_dipole = this.simulateur.dipoles[tempo.name];



								if (this.second_borne.object.name.includes("1"))
									this.signe_2 = "+";
								else
									this.signe_2 = "-";

								if (this.signe_1 === "+" && this.first_dipole.polePlus.length == 0){
									this.first_dipole.connecteASonPolePlus(this.second_dipole);
									draw = true;
								}else if (this.signe_1 === "-" && this.first_dipole.poleMoins.length == 0){
									this.first_dipole.connecteASonPoleMoins(this.second_dipole); 
									draw = true;
								}

								if (this.signe_2 === "+" && this.second_dipole.polePlus.length == 0){
									this.second_dipole.connecteASonPolePlus(this.first_dipole);
									draw = true;
								}else if (this.signe_2 === "-" && this.second_dipole.poleMoins.length == 0) {
									this.second_dipole.connecteASonPoleMoins(this.first_dipole); 
									draw = true;
								}

								if (draw){
									var cable = PRIMS3D.CablerElements(this.first_borne.point,this.second_borne.point);
									this.simulateur.scene.add(cable);
									this.list_cables.push(cable);
									this.first_borne.object.nbr_cables++;
									this.second_borne.object.nbr_cables++;
								}

								this.first_borne = null;
								this.second_borne = null;

								this.first_dipole = null;
								this.second_dipole = null;

								this.signe_1 = null;
								this.signe_2 = null;

								draw = false;


								console.log(this.simulateur);
							}
						} else {
							// console.log("you can't put more than 2 cables ")
							this.first_borne = null;
							this.second_borne = null;
						}
					}
				}
			}
		}
	}
}

ControleurCamera.prototype.activerClavier = function(){
	var that = this ;
	window.addEventListener('keyup',   function(e){that.keyUp(e);},   false) ;
	window.addEventListener('keydown', function(e){that.keyDown(e);}, false) ;
}

export default ControleurCamera ;
