
import * as THREE from '../three/r120/build/three.module.js' ;
import ControleurCamera from './controleur.js' ;
import PRIMS3D from './prims3d.js' ;


function Visu(){
	this.renderer   = null ;
	this.camera     = null ;
	this.scene      = null ;
	this.controleur = null ;
	this.horloge    = 0.0 ;
	this.chrono     = null ;
	this.dipoles     = {};
	this.generateur = null;
	this.circuit = [];
	this.acteurs    = [] ;
	this.Zt		 = 0;
	this.I		 = 0;
}

Visu.prototype.init = function(){
	var rd = new THREE.WebGLRenderer({antialias:true, alpha:true}) ;
	rd.setSize(window.innerWidth, window.innerHeight) ;
	document.body.appendChild(rd.domElement) ;

	var cam = new THREE.PerspectiveCamera(
				60.0,
				window.innerWidth / window.innerHeight,
				0.1, 1000.0) ;
	cam.position.set(5,1.7,5) ;
	cam.lookAt(new THREE.Vector3(0.0,1.7,0.0)) ;

	var ctl = new ControleurCamera(this,cam) ;

	var scn = new THREE.Scene() ;
	scn.add(new THREE.AmbientLight(0xffffff, 0.8)) ;
	var light = PRIMS3D.creerSourcePonctuelle(0xffffff, 1, 10, 1);
	light.position.set( 5, 6, 9 );
	scn.add( light );

	var chr = new THREE.Clock() ;

	this.scene = scn ;
	this.camera = cam ;
	this.controleur = ctl ;
	this.renderer = rd ;
	this.chrono = chr ;

	//crosshair

	var material = new THREE.LineBasicMaterial({ color: 0x00FF00 });
	// crosshair size
	var x = 0.01, y = 0.01;
	var geometry = new THREE.Geometry();
	// crosshair
	geometry.vertices.push(new THREE.Vector3(0, y, 0));
	geometry.vertices.push(new THREE.Vector3(0, -y, 0));
	geometry.vertices.push(new THREE.Vector3(0, 0, 0));
	geometry.vertices.push(new THREE.Vector3(x, 0, 0));
	geometry.vertices.push(new THREE.Vector3(-x, 0, 0));

	var crosshair = new THREE.Line( geometry, material );

    // place it in the center
    var crosshairPercentX = 50;
	var crosshairPercentY = 50;
	var crosshairPositionX = (crosshairPercentX / 100) * 2 - 1;
	var crosshairPositionY = (crosshairPercentY / 100) * 2 - 1;

	crosshair.position.x = crosshairPositionX * cam.aspect;
	crosshair.position.y = crosshairPositionY;
	crosshair.position.z = -0.3;

    cam.add( crosshair );
    scn.add( cam );

	return this ;

}

Visu.prototype.callbacks = function(){

	var that = this ;

	window.addEventListener('resize',
		function(){
			that.camera.aspect = window.innerWidth / window.innerHeight ;
			that.camera.updateProjectionMatrix() ;
			that.renderer.setSize(window.innerWidth, window.innerHeight) ;
		}) ;

	return this ;
}

Visu.prototype.go = function(){
	this.chrono.start() ;
	this.animer() ;
}

Visu.prototype.animer = function(){
	var that     = this ;

	var dt       = this.chrono.getDelta() ;
	this.horloge += dt ;
	requestAnimationFrame(function(){that.animer();}) ;
	this.controleur.update(dt) ;

	this.acteurs.forEach(function(x){x.update(dt);}) ;

	// Construction du circuit
	for(const nom in this.dipoles){
		if ( ( (this.dipoles[nom]).poleMoins.length + (this.dipoles[nom]).poleMoins.length == 2) &&  !(this.dipoles[nom]).iscircuit ){
			this.circuit.push(this.dipoles[nom]);
			this.Zt += this.dipoles[nom].Z;
			(this.dipoles[nom]).iscircuit = true;
		}
	}

	// Mise à jour des dipôles
	if (this.circuit.length > 2 && !this.dipoles["Interrupteur"].ouvert){
		for(var i=0; i<this.circuit.length;i++){

			if (this.I == 0)
				this.I = this.dipoles["Generateur"].U / this.Zt;
			this.circuit[i].I = this.I;
			if (this.circuit[i].nom != "Generateur")
				this.circuit[i].U = this.circuit[i].Z * this.I;
			this.circuit[i].update(dt) ;  
		
			//console.log(this.dipoles[nom]);
		}
	}
		if (this.dipoles["Interrupteur"].ouvert){
			this.I = 0;
			for(var i=0; i<this.circuit.length;i++){
			this.circuit[i].I = this.I;
			if (this.circuit[i].nom != "Generateur")
				this.circuit[i].U = this.circuit[i].Z * this.I;
			this.circuit[i].update(dt) ;  
		
			//console.log(this.dipoles[nom]);
			}
		}
			


	this.renderer.render(this.scene, this.camera) ;

}

Visu.prototype.ajouterActeur = function(unActeur){
	this.acteurs.push(unActeur) ;
}


export default Visu ;
