import * as THREE from '../three/r120/build/three.module.js' ;

import PRIMS3D from "./prims3d.js" ;

// ===============================================================================
// ================ DIPOLE
// ===============================================================================

function Dipole(nom,data,visu){

	data = data || {} ; 

	this.nom     = nom ; 
	
	this.visu= visu; 

	this.objet3d = null ; 

	this.Z = data.Z || 0.0  ; 
	this.U = data.U || 0.0 ; 
	this.I = data.I || 0.0 ; 
	this.Zt = 0.0;

	this.polePlus  = [] ; 
	this.poleMoins = [] ;

	this.iscircuit = false;

} 

Dipole.prototype.placer = function(x,y,z){
	if(this.objet3d){
		this.objet3d.position.set(x,y,z) ; 
	}
}

Dipole.prototype.orienter = function(rx,ry,rz){
	if(this.objet3d){
		this.objet3d.rotation.set(rx,ry,rz) ; 
	}
}

Dipole.prototype.connecteASonPolePlus = function(dipole){this.polePlus.push(dipole);}
Dipole.prototype.connecteASonPoleMoins = function(dipole){this.poleMoins.push(dipole);}

Dipole.prototype.react = function(msg, data){}

Dipole.prototype.conversion = function(){}

Dipole.prototype.update = function(dt){}

// ===============================================================================
// ================ GENERATEUR
// ===============================================================================

function Generateur(nom, data, visu,parent){
	
	Dipole.call(this, nom, data,visu) ; 

	data = data || {} ; 

	this.U = data.U || 4.5 ; 

	// Instructions pour associer au dipole la représentation 3d d une pile
	var boite_pile = PRIMS3D.creerBoiteElec("boite_pile", {image : "pile.png"});
	PRIMS3D.placer(boite_pile,1.0,0.1,0);
	parent.add(boite_pile);
	var pile = PRIMS3D.chargerOBJ(nom,"assets/obj/pile/","battery.obj","battery.mtl") ;
	PRIMS3D.placer(pile,0,-0.1,-0.08);
	boite_pile.add(pile);

	this.objet3d = boite_pile;
	
	
	visu.dipoles[nom] = this ; 
}

Generateur.prototype = Object.create(Dipole.prototype) ; 
Generateur.prototype.constructor = Generateur ;

// ===============================================================================
// ================ MOTEUR
// ===============================================================================

function Moteur(nom,data,visu,parent){

	Dipole.call(this,nom,data,visu) ; 

	this.vitesseAngulaire = 10.0 ; 

	// Création de l'incarnation
	var boite_moteur = PRIMS3D.creerBoiteElec("boite_moteur", {image : "moteur.png"});
	PRIMS3D.placer(boite_moteur,-1,1.1,0);
	parent.add(boite_moteur);

	var moteur = PRIMS3D.chargerOBJ(nom,"assets/obj/moteur/","motor_dc.obj",'motor_dc.mtl') ;
	boite_moteur.add(moteur) ;
	var rotor = PRIMS3D.chargerOBJ("disque","assets/obj/moteur/","cylinder.obj",'cylinder.mtl') ;
	moteur.add(rotor) ;

	this.objet3d = boite_moteur;
	
	
	this.rotor = rotor ; 
	
	visu.dipoles[nom] = this ; 

}

Moteur.prototype = Object.create(Dipole.prototype) ; 
Moteur.prototype.constructor = Moteur ; 

Moteur.prototype.conversion = function(){
	// Règle de conversion "bidon" pour test
	
	var W = this.U * this.I ; 
	//console.log("Conversion moteur: U="+this.U+", I="+this.I+", W="+W);
	if(Math.abs(W) > 0.001){
		this.vitesseAngulaire = 6*Math.PI*W ;
	} else {
		this.vitesseAngulaire = 0.0 ; 
	} 
}

Moteur.prototype.update = function(dt){
	// Calcul de la tension aux bornes (on suppose que l'intensité a déjà été calculée
	// this.U = this.Z * this.I ; 
	// Conversion
	this.conversion() ; 
	//console.log("animation moteur: "+this.vitesseAngulaire*dt);
	// Modification de l'incarnation en fonction des variables d'états : 
	this.rotor.rotation.y += this.vitesseAngulaire*dt ; 
}

// ===============================================================================
// ================ LAMPE
// ===============================================================================

function Lampe(nom,data,visu,parent){

	Dipole.call(this,nom,data,visu) ; 

	// this.fluxLumineux = 10.0 ;

	// Création de l'incarnation
	//Boite de l'ampoule OFF
	this.boite_ampoule_off = PRIMS3D.creerBoiteElec("boite_ampoule_off", {image : "ampoule.png"});
	PRIMS3D.placer(this.boite_ampoule_off,1.5,1.1,0);
	parent.add(this.boite_ampoule_off);

	var ampoule = PRIMS3D.chargerOBJ(nom,"assets/obj/ampoule/","lamp.obj","lamp_off.mtl") ;
	PRIMS3D.placer(ampoule,0,0.3,-0.08);
	this.boite_ampoule_off.add(ampoule);
	
	this.ampoule = ampoule ;

	this.ampoule_off = PRIMS3D.chargerOBJ("ampoule","assets/obj/ampoule/","lamp.obj","lamp_off.mtl");
	this.ampoule_on = PRIMS3D.chargerOBJ("ampoule","assets/obj/ampoule/","lamp.obj","lamp_on.mtl");

	visu.dipoles[nom] = this ; 


}

Lampe.prototype = Object.create(Dipole.prototype) ; 
Lampe.prototype.constructor = Moteur ; 

Lampe.prototype.conversion = function(){
	// Comportement juste pour tester
	//const W = this.U * this.I ; 
	//this.fluxLumineux = Math.min(1.0, W)  ; 
}

Lampe.prototype.update = function(dt){
	// const l = Math.min(1.0, this.fluxLumineux/10.0) ;
	// const l = Math.sin(this.visu.horloge) ; 
	// const l = Math.min(1.0, 6*this.fluxLumineux) ; 
	// this.ampoule.material.color.setHSL(50/360.0,1.0,l) ;
	var W = this.U * this.I ;
	console.log("W: "+W);
	if(Math.abs(W) > 0.001){
	console.log("ana hna");
		PRIMS3D.placer(this.ampoule_on,0,0.3,-0.08);
		this.boite_ampoule_off.remove(this.ampoule_off);
		this.boite_ampoule_off.add(this.ampoule_on);

	} else{
	console.log("ana lhih");
		PRIMS3D.placer(this.ampoule_off,0,0.3,-0.08);
		this.boite_ampoule_off.remove(this.ampoule_on);
		this.boite_ampoule_off.add(this.ampoule_off);
	} 
}

// ===============================================================================
// ================ Interrupteur
// ===============================================================================

function Interrupteur(nom,data,visu,parent){

	Dipole.call(this,nom,data) ; 
	this.ouvert = true ; 
    this.chrono = 1.0 ; 

	// Création de l'incarnation de l'interrupteur
	var boite_interrupteur = PRIMS3D.creerBoiteElec("boite_interrupteur",{image : "interrupteur.png"});
	var interrupteur = PRIMS3D.creerInterrupteur(nom,{});
	boite_interrupteur.add(interrupteur);
	PRIMS3D.placer(boite_interrupteur,0.5,2.1,0);
	parent.add(boite_interrupteur);

	this.objet3d = boite_interrupteur;
	
	visu.dipoles[nom] = this ; 


}

Interrupteur.prototype = Object.create(Dipole.prototype) ; 
Interrupteur.prototype.constructor = Interrupteur ; 

Interrupteur.prototype.react = function(msg,data){this.ouvert = ! this.ouvert;}
Interrupteur.prototype.update = function(dt){

    // ======================================
    // Logique temporaire de changement d'état
    this.chrono -= dt ; 
    if(this.chrono < 0.0){
        //console.log("CHRONO : ",this.chrono) ; 
        this.chrono = 1.0 ; 
        //this.ouvert = ! this.ouvert ; 
    }
    // =======================================
    
	if(this.ouvert){
		this.Z = 10000000 ;
		// Action sur l'incarnation pour qu'elle représente l'interrupteur ouvert
	} else {
		this.Z = 0.0 ;  
		// Action sur l'incarnation pour qu'elle représente l'interrupteur ouvert
	}
}	

	


const DIPOLES = {
	"Dipole" : Dipole,
	"Generateur" : Generateur,
	"Interrupteur" : Interrupteur,
	"Lampe" : Lampe,
	"Moteur" : Moteur

}

export default DIPOLES
