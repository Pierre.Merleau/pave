
import * as THREE from  '../three/r120/build/three.module.js' ;
import PRIMS3D from './prims3d.js' ;
import DIPOLES    from './dipole.js' ; 
function Fabrique(visu){
	this.visu = visu ;
	this.scene = visu.scene ;
	this.etagere = null;
	this.annuaire = {} ;
}


Fabrique.prototype.fabriquerDecor = function(){

	this.scene.add(new THREE.GridHelper(100,10)) ;
	this.scene.add(new THREE.AxesHelper(5)) ;

	// var mat1 = PRIMS3D.creerMateriau({couleur:0xff00ff}) ;
	// var mat2 = PRIMS3D.creerMateriau({couleur:0xffff00}) ;
	var greyMat = PRIMS3D.creerMateriau({couleur:0x707070}) ;

	var matPlafond = PRIMS3D.creerMateriauTexture("assets/textures/murs/wallOut.jpg",0xffffff,5,5);
  	var matSol = PRIMS3D.creerMateriauTexture("assets/textures/sols_plafonds/solCarrelage.jpg",0xffffff,2.5,2.5);
  	var matTable = PRIMS3D.creerMateriauTexture("assets/textures/240.jpg",0xffffff,1,1.5);
	var matEtagere = PRIMS3D.creerMateriauTexture("assets/textures/sols_plafonds/parquet1.jpg",0xffffff);
	var matEtagere2 = PRIMS3D.creerMateriauTexture("assets/textures/sols_plafonds/parquetRotation.jpg",0xffffff);
	var matMur = PRIMS3D.creerMateriauTexture("assets/textures/murs/whiteWall1.jpg",0xffffff,2,1);


	var sol = PRIMS3D.creerSol("sol",{largeur:12, profondeur:15, materiau:matSol});
	sol.position.set(6,0,7.5);
	this.scene.add(sol) ;

	var mur1 = PRIMS3D.creerCloison("mur1",
					{
						largeur:12,
                        hauteur:5,
                        epaisseur:0.1,
                        materiau: matMur
					}) ;
	mur1.position.set(6,0,0);
	this.scene.add(mur1) ;

  var mur2 = PRIMS3D.creerCloison("mur2",
  {
      largeur:15,
      hauteur:5,
      epaisseur:0.1,
      materiau: matMur
  }) ;
	mur2.position.set(0,0,7.5);
  mur2.rotateY(1.57);
  this.scene.add(mur2);

  var mur3 = PRIMS3D.creerCloison("mur3",
  {
      largeur:12,
      hauteur:5,
      epaisseur:0.1,
      materiau: matMur
  }) ;
  mur3.position.set(6,0,15)
  this.scene.add(mur3);

  var mur4 = PRIMS3D.creerCloison("mur4",
      {
          largeur:15,
          hauteur:5,
          epaisseur:0.1,
          materiau: matMur
      }) ;
  mur4.position.set(12,0,7.5)
  mur4.rotateY(-1.57);
  this.scene.add(mur4);

  var plafond = PRIMS3D.creerCloison("plafond",
      {
          largeur:12,
          hauteur:15,
          epaisseur:0.1,
          materiau: matPlafond
      }) ;
  plafond.rotateX(Math.PI/2)
  plafond.position.set(6,5,0)
  this.scene.add(plafond);


	var poster = PRIMS3D.creerPoster("poster",{largeur:4, hauteur:3,nomImage:"assets/images/schema.png"}) ;
	poster.position.set(8,2.5,0.1) ;
	this.scene.add(poster) ;

	var window = PRIMS3D.creerPoster("poster",{largeur:3, hauteur:2,nomImage:"assets/images/window.jfif"}) ;
	window.position.set(11.9,2.5,8) ;
	this.scene.add(window) ;
	PRIMS3D.orienter(window, - Math.PI/2.0) ;

	var door = PRIMS3D.creerPoster("poster",{largeur:1.8, hauteur:3.6,nomImage:"assets/images/door.png"}) ;
	door.position.set(7,1.7,14.9) ;
	this.scene.add(door) ;
	PRIMS3D.orienter(door, - Math.PI) ;

	//desserte
	var desserte = PRIMS3D.creerBoite("desserte",{largeur:1.1,hauteur:0.005,epaisseur:1.9,materiau:greyMat});
	PRIMS3D.placer(desserte,6.1,1.03,4.005);
	this.scene.add(desserte);

	//table
	var table = PRIMS3D.creerGroupe("table") ;

	var plateau02 = PRIMS3D.creerBoite("plateau-table01",{largeur:5,hauteur:0.05,epaisseur:2,materiau:matTable}) ;
	PRIMS3D.placer(plateau02,0,1,0) ;
	table.add(plateau02) ;

	var pied1 = PRIMS3D.creerBoite("pied1",{largeur:0.05,hauteur:1,epaisseur:0.05,materiau:matTable}) ;
	PRIMS3D.placer(pied1, 2.2, 0.5, 0.5) ;
	table.add(pied1) ;


	var pied2 = PRIMS3D.creerBoite("pied2",{largeur:0.05,hauteur:1,epaisseur:0.05,materiau:matTable}) ;
	PRIMS3D.placer(pied2, 2.2, 0.5, -0.5) ;
	table.add(pied2) ;


	var pied3 = PRIMS3D.creerBoite("pied3",{largeur:0.05,hauteur:1,epaisseur:0.05,materiau:matTable}) ;
	PRIMS3D.placer(pied3, -2.2, 0.5, 0.5) ;
	table.add(pied3) ;


	var pied4 = PRIMS3D.creerBoite("pied4",{largeur:0.05,hauteur:1,epaisseur:0.05,materiau:matTable});
	PRIMS3D.placer(pied4, -2.2, 0.5, -0.5);
	table.add(pied4);

	PRIMS3D.placer(table, 8, 0, 4);
	this.scene.add(table);

	//etagere

	this.etagere = PRIMS3D.creerGroupe("etagere") ;

	var plateau02 = PRIMS3D.creerBoite("plateau-etagere",{largeur:4,hauteur:0.05,epaisseur:1.5,materiau:matEtagere2}) ;
	PRIMS3D.placer(plateau02,0,1,0) ;
	this.etagere.add(plateau02) ;

	var plateau01 = PRIMS3D.creerBoite("plateau-etagere",{largeur:4,hauteur:0.05,epaisseur:1.5,materiau:matEtagere2}) ;
	PRIMS3D.placer(plateau01,0,0,0) ;
	this.etagere.add(plateau01) ;

	var plateau03 = PRIMS3D.creerBoite("plateau-etagere",{largeur:4,hauteur:0.05,epaisseur:1.5,materiau:matEtagere2}) ;
	PRIMS3D.placer(plateau03,0,2,0) ;
	this.etagere.add(plateau03);

	var plateau08 = PRIMS3D.creerBoite("plateau-etagere",{largeur:4,hauteur:0.05,epaisseur:1.5,materiau:matEtagere2}) ;
	PRIMS3D.placer(plateau08,0,3,0) ;
	this.etagere.add(plateau08);

	// milieu
	var plateau04 = PRIMS3D.creerBoite("plateau-etagere",{largeur:0.05,hauteur:3,epaisseur:1.5,materiau:matEtagere}) ;
	PRIMS3D.placer(plateau04,0,1.5,0) ;
	this.etagere.add(plateau04);


	//limite a droite
	var plateau05 = PRIMS3D.creerBoite("plateau-etagere",{largeur:0.05,hauteur:3,epaisseur:1.5,materiau:matEtagere}) ;
	PRIMS3D.placer(plateau05,-2,1.5,0) ;
	this.etagere.add(plateau05);

	//limite a gauche
	var plateau06 = PRIMS3D.creerBoite("plateau-etagere",{largeur:0.05,hauteur:3,epaisseur:1.5,materiau:matEtagere}) ;
	PRIMS3D.placer(plateau06,2,1.5,0) ;
	this.etagere.add(plateau06);

	//limite fond
	var fond = PRIMS3D.creerBoite("plateau-etagere",{largeur:0.05,hauteur:3,epaisseur:4,materiau:greyMat}) ;
	PRIMS3D.placer(fond,0,1.5,-0.5);
	this.etagere.add(fond);
	PRIMS3D.orienter(fond, Math.PI/2.0);

	PRIMS3D.placer(this.etagere,2.5,0,2) ;
	PRIMS3D.orienter(this.etagere, Math.PI/4.0) ;

	this.scene.add(this.etagere);


	// // Pose de boites vide sur l'etagere pour test
	// // TODO : remove that later




}

Fabrique.prototype.fabriquerDipole = function(){
	//Insérer les appels aux constructeurs des dipoles
	var generateur = new DIPOLES.Generateur("Generateur",{U:5,Z:0.5},this.visu,this.etagere);
	var moteur = new DIPOLES.Moteur("Moteur",{Z:1},this.visu,this.etagere);
	var lamp = new DIPOLES.Lampe("Lampe",{Z:0.5},this.visu,this.etagere);
	var interrupteur = new DIPOLES.Interrupteur("Interrupteur",{},this.visu,this.etagere);

	//ajout generateur au simulateur

	this.visu.generateur = generateur;
	

	//Pose d'une autre boite pour la résistance sur l'etagere
	var boite_resistance = PRIMS3D.creerBoiteElec("boite_resistance", {image : "resistance.png"});
	PRIMS3D.placer(boite_resistance,-1,2.1,0);
	this.etagere.add(boite_resistance);

	//Pose d'une autre boite pour la DEL sur l'etagere
	var boite_led = PRIMS3D.creerBoiteElec("boite_led", {image : "del.png"});
	PRIMS3D.placer(boite_led,1.5,2.1,0);
	this.etagere.add(boite_led);


	//Résistance
	var resistance = PRIMS3D.chargerOBJ("resistance","assets/obj/resistance/","model.obj","resistance.mtl") ;
	boite_resistance.add(resistance);

	//DEL
	var del = PRIMS3D.chargerOBJ("del","assets/obj/DEL/","DEL.obj","DEL.mtl") ;
	boite_led.add(del)



	//cable etagere
	var cabledemo = PRIMS3D.creerGroupe("cabledemo") ;
	var matEtagere = PRIMS3D.creerMateriauTexture("assets/textures/sols_plafonds/parquet1.jpg",0xffffff);
	var boiteCable = PRIMS3D.creerBoite("boiteCable",{largeur:0.2,hauteur:0.2,epaisseur:1.5,materiau:matEtagere}) ;
	cabledemo.add(boiteCable);
	PRIMS3D.placer(cabledemo,0.2,2.5,5) ;

	this.scene.add(cabledemo);

	for (var i = 0; i < 12; i++) {
		var decal = (i*0.1)-0.6;
		var cbl = PRIMS3D.creerCable("cable_"+i,{});
		PRIMS3D.placer(cbl,0,-0.8,decal);
		cabledemo.add(cbl);
	}
}



Fabrique.enregistrer = function(nom,objet){
				this.annuaire[nom] = objet ;
			}

Fabrique.chercher = function(nom,defaut){
				return this.annuaire[nom] || defaut ;
			}


export default Fabrique ;
