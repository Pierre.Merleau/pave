
import * as THREE from '../three/r120/build/three.module.js' ; 

var ACTEURS = {} ;

ACTEURS.Traj = function(nom,options){
	this.nom      = nom ; 
	this.objet    = options.objet || null ; 
	var chemin    = options.path  || [] ;
	this.path     = new THREE.CatmullRomCurve3(chemin) ;  
	this.pos      = 0.0 ; 
	this.velocite = options.velocite || 1.0 ; 
	this.factor   = this.velocite / this.path.getLength() ; 
}

ACTEURS.Traj.prototype.update = function(dt){
	if(this.objet){
		this.objet.position.copy(this.path.getPointAt(this.pos)) ; 
		this.pos += this.factor*dt ; 
		if(this.pos>1) this.pos = 0 ; 
		this.objet.lookAt(this.path.getPointAt(this.pos)) ; 
	}
}	

export default ACTEURS ; 
