
import * as THREE from '../three/r120/build/three.module.js' ;
import {OBJLoader} from '../three/r120/examples/jsm/loaders/OBJLoader.js';
import {MTLLoader} from '../three/r120/examples/jsm/loaders/MTLLoader.js';

var PRIMS3D = {} ;

PRIMS3D.materiauBlanc = new THREE.MeshLambertMaterial({color:0xffffff}) ;
PRIMS3D.materiauRouge = new THREE.MeshLambertMaterial({color:0xff0000}) ;
PRIMS3D.materiauGris = new THREE.MeshLambertMaterial({color:0xa9a9a9}) ;

PRIMS3D.creerGroupe = function(nom){
	var groupe = new THREE.Group() ;
	groupe.name = nom ;
	return groupe ;
}

PRIMS3D.creerCable = function(nom,options){
	var hauteur        = options.hauteur       || 2.0 ;
	var materiau     = options.materiau     || PRIMS3D.materiauRouge ;
	var materiauPin = PRIMS3D.materiauGris;

	var cable= new THREE.Group();
	var geo = new THREE.CylinderGeometry( 0.01, 0.01, 2, 32);
	var mesh = new THREE.Mesh( geo, materiau );
	mesh.name="fil";
	cable.add(mesh);

	var geo = new THREE.CylinderGeometry( 0.02, 0.02, 0.1, 32);
	var mesh = new THREE.Mesh( geo, materiau );
	mesh.position.set(0,1,0)
	mesh.name="connecteur1";
	cable.add(mesh);
	var geo = new THREE.CylinderGeometry( 0.02, 0.02, 0.1, 32);
	var mesh = new THREE.Mesh( geo, materiau );
	mesh.position.set(0,-1,0)
	mesh.name="connecteur2";
	cable.add(mesh);

	var geo = new THREE.CylinderGeometry( 0.01, 0.01, 0.1, 32);
	var mesh = new THREE.Mesh( geo, materiauPin );
	mesh.position.set(0,-1.1,0)
	mesh.name="pin1";
	cable.add(mesh);

	var geo = new THREE.CylinderGeometry( 0.01, 0.01, 0.1, 32);
	var mesh = new THREE.Mesh( geo, materiauPin );
	mesh.position.set(0,1.1,0)
	mesh.name="pin2";
	cable.add(mesh);

	return cable;
}

PRIMS3D.creerBoite = function(nom,options){
	var largeur    = options.largeur   || 1.0 ;
	var hauteur    = options.hauteur   || 1.0 ;
	var epaisseur  = options.epaisseur || 1.0 ;
	var materiau   = options.materiau  || PRIMS3D.materiauBlanc ;

	var geo  = new THREE.BoxGeometry(largeur, hauteur, epaisseur) ;
	var mesh = new THREE.Mesh(geo, materiau) ;
	mesh.name = nom ;
	return mesh ;
}


PRIMS3D.creerSphere = function(nom,options){
	var rayon        = options.rayon        || 1.0 ;
	var subdivisions = options.subdivisions || 16 ;
	var materiau     = options.materiau     || PRIMS3D.materiauBlanc ;

	var geo  = new THREE.SphereGeometry(rayon, subdivisions, subdivisions) ;
	var mesh = new THREE.Mesh(geo, materiau) ;
	mesh.name = nom ;
	return mesh ;
}


PRIMS3D.creerBoiteElec = function(nom,options){
	var largeur    = options.largeur   || 0.4 ;
	var hauteur    = options.hauteur   || 0.1 ;
	var epaisseur  = options.epaisseur || 0.3 ;
	var materiauBoite   = options.materiau || PRIMS3D.materiauBlanc ;
	var materiauFiche = options.materiauFiche || PRIMS3D.materiauRouge ;
	var path = "assets/textures/schema_boite/"
	var imgBoite = options.image || "default.png"
	imgBoite = path+imgBoite; 
	var resultat= new THREE.Group();
	var spot = options.spot || "étagère";

	var geo  = new THREE.BoxGeometry(largeur, hauteur, epaisseur) ;
	var mesh = new THREE.Mesh(geo, materiauBoite) ;
	mesh.name = "boite" ;
	resultat.add(mesh);

	var geo = new THREE.CylinderGeometry( 0.03, 0.03, hauteur+0.01, 32);
	var mesh = new THREE.Mesh( geo, materiauFiche );
	mesh.position.set(largeur/2-0.05,0,0)
	mesh.name="borne1_"+nom;
	mesh.type="borne";
	mesh.nbr_cables=0;
	resultat.add(mesh);

	var geo = new THREE.CylinderGeometry( 0.03, 0.03, hauteur+0.01, 32);
	var mesh = new THREE.Mesh( geo, materiauFiche );
	mesh.position.set(-largeur/2+0.05,0,0)
	mesh.type="borne"
	mesh.name="borne2_"+nom;
	mesh.nbr_cables=0;
	resultat.add(mesh);


	var geo    = new THREE.PlaneGeometry(2/12,1/12,1,1) ;
	var mat    = PRIMS3D.creerMateriauTexture(imgBoite, 0xffffff) ;
	var mesh   = new THREE.Mesh(geo, mat) ;
	mesh.rotation.x = - Math.PI / 2 ;
	mesh.position.set(0,hauteur-0.04,0.08);
    mesh.name  = "image_boite";
    resultat.add(mesh);


	resultat.name=nom;
	resultat.spot=spot;
    resultat.type="boite_elec";


	return resultat ;
}

PRIMS3D.creerInterrupteur = function(nom,options){
	var materiauBouton = options.materiauBouton || PRIMS3D.materiauGris ;
	var resultat= new THREE.Group();
	resultat.name=nom;
	//var on = 1;

	var boite = PRIMS3D.creerBoiteElec("boite_test",{image : "interrupteur.png"});
	resultat.add(boite);

	var geo = new THREE.CylinderGeometry( 0.02, 0.005, 0.4, 32);
	var mesh = new THREE.Mesh( geo, materiauBouton );
	mesh.position.set(0,0.01,0);
	mesh.rotation.z = Math.PI/4;
	mesh.name="bouton";
	mesh.on=1;
	resultat.add(mesh);
	return resultat ;
}


PRIMS3D.creerSol = function(nom,options){
	var largeur    = options.largeur    || 1.0 ;
	var profondeur = options.profondeur || largeur ;
	var materiau   = options.materiau   || PRIMS3D.materiauBlanc ;

	var geo   = new THREE.PlaneGeometry(
					largeur,profondeur,
					Math.floor(largeur/10.0)+1, Math.floor(profondeur/10)+1) ;
	var mesh  = new THREE.Mesh(geo,materiau) ;
	mesh.name = nom ;
	mesh.rotation.x = - Math.PI / 2 ;
	return mesh ;
}

PRIMS3D.creerCloison = function(nom,options){
	var largeur   = options.largeur   || 1.0 ;
	var hauteur   = options.hauteur   || 1.0 ;
	var epaisseur = options.epaisseur || 1.0 ;
	var materiau  = options.materiau  || PRIMS3D.materiauBlanc ;

	var geo  = new THREE.BoxGeometry(largeur, hauteur, epaisseur) ;
	var mesh = new THREE.Mesh(geo, materiau) ;
	var groupe = new THREE.Group() ;
	groupe.name = nom ;
	groupe.add(mesh) ;
	mesh.position.set(0,hauteur/2.0,0) ;
	return groupe ;
}

PRIMS3D.creerPoster = function(nom,options){
	var largeur = options.largeur || 1.0 ;
	var hauteur = options.hauteur || 1.0 ;
	var nomImage = options.nomImage ;
	var geo    = new THREE.PlaneGeometry(largeur, hauteur) ;
	var mat    = PRIMS3D.creerMateriauTexture(nomImage, 0xffffff) ;
	var mesh   = new THREE.Mesh(geo, mat) ;
    	mesh.name  = "poster_"+nom ;
	var dos    = new THREE.Mesh(geo, PRIMS3D.materiauBlanc) ;
	dos.rotation.y = Math.PI ;
	dos.position.z = -0.01 ;
	mesh.position.z = 0.01 ;

	var groupe = new THREE.Group() ;
	groupe.add(mesh) ;
	groupe.add(dos) ;
	groupe.name  = nom ;
	return groupe ;
}

/*
PRIMS3D.chargerObj = function(nom,repertoireObj,nomObj,repertoireMtl,nomMtl){
			var mtlLoader = new THREE.MTLLoader() ;
			var objLoader = new THREE.OBJLoader() ;
			var groupe    = new THREE.Group() ;
			groupe.name = nom ;
			mtlLoader.setTexturePath(repertoireMtl);
			mtlLoader.setPath(repertoireMtl);
			mtlLoader.load(nomMtl, function (materials) {

    				materials.preload();

    				objLoader.setMaterials(materials);
    				objLoader.setPath(repertoireObj);
    				objLoader.load(nomObj, function (object) {
        				groupe.add(object);
					object.name = nom ;
					//return groupe ;

    				});

			});

			return groupe ;

}
*/
PRIMS3D.chargerOBJ = function(nom,repertoire,nomObj,nomMtl){
	const resultat = new THREE.Group() ;
	resultat.name=nom;
	
	const objLoader = new OBJLoader() ;
	const mtlLoader = new MTLLoader() ;
	mtlLoader.setPath(repertoire) ;
	mtlLoader.load(nomMtl, function(materials){
		materials.preload() ;
		objLoader.setMaterials(materials);
		objLoader.setPath(repertoire) ;
		objLoader.load(nomObj, function(obj){
			resultat.add(obj);
		})
	}) ;




	return resultat ;
}




// ====================
// Traitement de meshes
// ====================

PRIMS3D.placer = function(mesh,x,y,z){
	mesh.position.set(x,y,z)  ;
}

PRIMS3D.orienter = function(mesh,y){
	mesh.rotation.y = y ;
}


PRIMS3D.fixerEchelleMesh = function(mesh,sx,sy,sz){
	mesh.scale.set(sx,sy,sz) ;
}

PRIMS3D.placerSous = function(fils,pere){
	pere.add(fils) ;
}



// ===================
// Création de sources
// ===================

PRIMS3D.creerSourcePonctuelle = function(couleur, intensite, portee, attenuation){
	var light = new THREE.PointLight(couleur,intensite,portee,attenuation) ;
	return light ;
}

PRIMS3D.creerSoleil = function(intensite){
	var h = new THREE.HemisphereLight(0xffffbb,0x080820,intensite) ;
	return h ;
}

// =====================
// Création de matériaux
// =====================

/*
var materiauBlanc  = creerLambert(0xffffff) ;
var materiauRouge  = creerLambert(0xff0000) ;
var materiauVert   = creerLambert(0x00ff00) ;

var materiauParquet = creerLambertTexture("assets/textures/sol/parquet1.jpg",0xffffff,10,10) ;
var materiauDante   = creerLambertTexture("assets/textures/murs/dante.jpg",0xffffff,1,1) ;
var materiauBrique  = creerLambertTexture("assets/textures/murs/bricks3.jpg",0xffffff,6,2) ;
*/

PRIMS3D.creerMateriau = function(options){
	var couleur = options.couleur || 0xffffff ;
  	var mat = new THREE.MeshStandardMaterial({color:couleur}) ;
	return mat ;
}

PRIMS3D.creerMateriauTexture = function(nomImage,couleur,nx,ny){
	var textureLoader = new THREE.TextureLoader() ;
	var texture = textureLoader.load(nomImage) ;
	var mat = new THREE.MeshStandardMaterial({color:couleur,map:texture}) ;
	nx = nx ||   1 ;
	ny = ny ||   1 ;
	mat.map.wrapS = THREE.RepeatWrapping ;
	mat.map.wrapT = THREE.RepeatWrapping ;
	mat.map.repeat.set(nx,ny) ;
	return mat ;
}
PRIMS3D.CablerElements=function(point1,point2){

	var curve = new THREE.CatmullRomCurve3( [
		new THREE.Vector3( point1.x, point1.y+0.01, point1.z ),
		//Ajout d'un point pour creer une jolie courbure
		new THREE.Vector3((point1.x+point2.x)/2, point1.y+0.01, (point1.z+point2.z)/2+0.05*(point1.z+point2.z)/2 ),
		new THREE.Vector3( point2.x, point2.y+0.01, point2.z ),
	] );

	var points = curve.getPoints( 50 );
	var geometry = new THREE.BufferGeometry().setFromPoints( points );
	var material = new THREE.LineBasicMaterial( { color : 0xff0000 } );

	// Create the final object to add to the scene
	var cable = new THREE.Line( geometry, material );
	return cable;
}

export default PRIMS3D ;
